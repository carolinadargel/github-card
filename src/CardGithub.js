import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { findByLabelText } from '@testing-library/react';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
        minHeight: 445,
        position: findByLabelText,
    },
});

export default function CardGithub(props) {

    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    alt=""
                    height="400"
                    image={props.img}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {props.name}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        <h3>{props.location}</h3>
                        <p>{props.bio}</p>
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}