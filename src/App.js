import React, { Component } from 'react';
import './App.css';
import Button from '@material-ui/core/Button';
import CardGithub from './CardGithub.js';



const PROFILE_URL = "https://api.github.com/users/maria";

class App extends Component {

  state = {
    active: false,
    user: {}
  };


  getInitialState = () => {
    return this.setState({ active: false })
  };

  handleToggle = () => {

    fetch(PROFILE_URL)
      .then((res) => {
        return res.json()
      }).then((data) => this.setState({ user: data, active: true }))


  };

  ChangeState = () => {

    this.setState({ active: false })
  }


  // dica: é aqui que o state.active pode ser útil
  render() {

    console.log(this.state.user)

    if (this.state.active === false) {
      return (
        <div className="App">
          <Button
            onClick={() => this.handleToggle()}
            variant="contained"
          >Perfil do Usuário</Button>
        </div>
      )
    }

    return (

      <div className="App">

        <Button variant="contained" onClick={this.ChangeState}>

          Ocultar Perfil do Usuário
       </Button>

        <div className="user">

          <CardGithub img={this.state.user.avatar_url}
            alt={""}
            name={this.state.user.name}
            location={this.state.user.location}
            bio={this.state.user.bio}
          />


        </div>

      </div>

    );
  }
}

export default App;
